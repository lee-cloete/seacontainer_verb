<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


function remove_menus(){
  remove_menu_page( 'index.php' );              //Dashboard
  remove_menu_page( 'edit.php' );               //Posts
  remove_menu_page( 'edit-comments.php' );			//Comments
  remove_menu_page( 'tools.php' );              //Tools
  // remove_menu_page( 'options-general.php' );
  // remove_menu_page( 'edit.php?post_type=acf-field-group' );
  // remove_menu_page( 'admin.php?page=cptui_manage_post_types' );
}
add_action( 'admin_menu', 'remove_menus' );


add_filter('admin_footer_text', 'wp_bootstrap_custom_admin_footer');
function wp_bootstrap_custom_admin_footer() {
  echo '<span id="footer-thankyou">Developed by <a style="text-decoration:none;" href="mailto:hello@leecloete.com" target="_blank">Lee Cloete</a></span>';
}

// Removes from admin menu
add_action( 'admin_menu', 'my_remove_admin_menus' );
function my_remove_admin_menus() {
  remove_menu_page( 'edit-comments.php' );
}
// Removes from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
  remove_post_type_support( 'post', 'comments' );
  remove_post_type_support( 'page', 'comments' );
}

//SVG Upload
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter('use_block_editor_for_post_type', '__return_false', 10);

add_filter("gform_validation_message", "gwp_change_message", 10, 2);
function gwp_change_message($message, $form){
return '
<div class="validation_error">Please Enter a Valid Email Address</div>
';
}