<footer class="content-info">
  <div class="container">
      <div class="row">
            <div class="col-xs-6 col-sm-6 col-lg-3 col-md-3 col-xl-3 spacer-m">
              <h4> Address</h4>
              <div class="footer-hold">
                  <p>20 Upper Ground<br />
                  London, SE1 9PD <br />
                  44 (0) 20 3747 1000</p>
                  <i class="fab fa-instagram fa-lg"></i>
                  <i class="fab fa-facebook-f fa-lg"></i>
                  <i class="fab fa-twitter fa-lg"></i>
                  <i class="fab fa-tripadvisor fa-lg"></i>
                </div>
            </div> 

            <div class="col-xs-6 col-sm-6 col-lg-3 col-md-3 col-xl-3 spacer-m">
            <h4> Information </h4>
            <div class="footer-hold">
                <ul>
                  <li>Contact</li>
                  <li>Careers</li>
                  <li>FAQ's</li>
                  <li>Privacy</li>
                  <li>T&Cs</li>
                </ul> 
              </div>        
            </div> 

            <div class="col-xs-6 col-sm-6 col-lg-3 col-md-3 col-xl-3 form-hold spacer-m">
            <h4>Subscribe </h4>
              <p>We’ve got plenty of news, be the first to read all about it.</p>
              <?php gravity_form(1, false, false, false, '', true, 12); ?>
            </div> 

            <div class="col-xs-6 col-sm-6 col-lg-3 col-md-3 col-xl-3 spacer-m">
              <img class="mx-auto d-block brand-icon" src="/wp-content/uploads/2019/06/Slice-1.png" />
          </div> 


      </div>
  </div>
</footer>
<div class="copy_right">
  <div class="container">
    <div class="row align-items-center">

      <div class="col-4 text-left">
        &copy; 2018 Sea Containers
      </div>
    
      <div class="col-4 text-center">
        <img class="mx-auto d-block footer-icon" src="/wp-content/uploads/2019/06/icon.png" >
      </div>
    
      <div class="col-4 text-right">
        <a target="blank" href="https://verbbrands.com/">Luxury Digital Agency </a>
      </div>

    </div>
  </div>  
</div>  

<!-- Test Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">BOOK NOW</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
    </div>
  </div>
</div>
<!-- Test Modal -->
