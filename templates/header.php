<header class="banner">
  <div class="container">

     <div class="row align-items-center">
          <div class="col-4">
            <div id="nav-icon1">
                <span></span>
                <span></span>
                <span></span>
                <div class="menu_offset">
                  Menu
                </div>
              </div>
          </div>

          <div class="col-xs-8 col-sm-8 col-lg-4 col-md-4 col-xl-4">
              <img class="mx-auto d-block logo-size" src="/wp-content/uploads/2019/06/logo-white.png" />   
          </div>

          <div class="col-4 text-right hide_m">
              <div class="booking_modal">
                <a data-toggle="modal" data-target="#exampleModal" href="#">BOOK NOW</a>
              </div>   
          </div>
      </div>

  </div>
</header>

<div class="header_slider">
  <img class="center-block img-fluid" src="/wp-content/uploads/2019/06/header_main.png" />
</div>

<div class="filter-search">
    <div class="container">
      <div class="row filter_row">

      
        <!-- Select-->
          <div class="col-xs-6 col-sm-6 col-lg-2 col-md-2 col-xl-2">
            <label for="filter">
              <select id="filter" name="select">
                <option value="0">Check in</option>
                <option value="1">Date Picker</option>
                <option value="2">Here</option>
              </select>
            </label>
          </div>
        <!-- Select-->


        <!-- Select-->
        <div class="col-xs-6 col-sm-6 col-lg-2 col-md-2 col-xl-2">
            <label for="filter">
              <select id="filter" name="select">
                <option value="0">Check out</option>
                <option value="1">Date Picker</option>
                <option value="2">Here</option>
              </select>
            </label>
          </div>
        <!-- Select-->


        <!-- Select-->
        <div class="col-xs-6 col-sm-6 col-lg-2 col-md-2 col-xl-2">
            <label for="filter">
              <select id="filter" name="select">
                <option value="0">1 Adult(s)</option>
                <option value="1">Date Picker</option>
                <option value="2">Here</option>
              </select>
            </label>
          </div>
        <!-- Select-->


        <!-- Select-->
        <div class="col-xs-6 col-sm-6 col-lg-2 col-md-2 col-xl-2">
            <label for="filter">
              <select id="filter" name="select">
                <option value="0">0 Children</option>
                <option value="1">Date Picker</option>
                <option value="2">Here</option>
              </select>
            </label>
          </div>
        <!-- Select-->


        <!-- Select-->
        <div class="col-xs-6 col-sm-6 col-lg-2 col-md-2 col-xl-2">
            <label for="filter">
              <select id="filter" name="select">
                <option value="0">1 Room</option>
                <option value="1">Date Picker</option>
                <option value="2">Here</option>
              </select>
            </label>
          </div>
        <!-- Select-->


        <!-- Select-->
        <div class="col-xs-6 col-sm-6 col-lg-2 col-md-2 col-xl-2 filter_submit">
            <a role="button" href="#" class="btn btn-brand-primary">SEARCH</a>
          </div>
        <!-- Select-->

      </div>
    </div>
</div>

