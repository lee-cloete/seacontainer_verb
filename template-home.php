<?php
/**
 * Template Name: Home Page Template
 */
?>

<?php while (have_posts()) : the_post(); ?>

<section id="into">
  <header>
      <h1 class="entry-title">Sea Containers London The<br />distinctive South Bank hotel</h1>
  </header> 
  <hr />
  <p class="text-center">With design reminiscent of a 1920s transatlantic cruise liner, award-winning<br />  food and drink outlets, and a continuously bold energy, Sea<br /> Containers London is the distinctive South Bank hotel. It’s a destination<br /> where glamour meets brutalism – and it is your anchor on the Thames.</p>
</section>

<section class="standard_s">
  <div class="row">

      <div class="owl-carousel">

       <!-- slide -->
        <div> 
          <img class="mx-auto d-block img-fluid" src="/wp-content/uploads/2019/06/slider_test_2.jpg" /> 
        </div>
        <!-- slide -->
        <!-- slide -->
        <div> 
          <img class="mx-auto d-block img-fluid" src="/wp-content/uploads/2019/06/slider_test_2.jpg" /> 
        </div>  
         <!-- slide -->
  
      </div>
  </div>

  <div class="container">
      <div class="column-span">
        <p>When it comes to your quarters, we have applied the same attention to detail and design-led philosophy as with the rest of our hotel. A space to unwind, recharge and regroup, each of our guest rooms offers the ideal South Bank base from which to explore the Big Smoke. Whether you opt for one of our standard rooms, or book into a luxurious river view apartment suite, you’re guaranteed to have a night to remember. Award-winning designer Tom Dixon has turned his hand to all aspects of the rooms, perfecting every little detail from the artwork to the positioning of the beds. Featuring bespoke furniture, custom-designed dimmable lighting, and impressive marble bathrooms, our guest rooms are here to be your London abode.
        </p>  
        <div class="text-center">
            <a role="button" href="#" class="btn btn-brand-primary">View Accomdation</a>
        </div>
      </div>
      <hr />
  </div>
</section>  

<section class="standard_s">
  <div class="row">
    <div class="owl-carousel">
        <div> 
          <img class="mx-auto d-block img-fluid" src="/wp-content/uploads/2019/06/slider_test_1.jpg" /> 
        </div>
        <div> 
          <img class="mx-auto d-block img-fluid" src="/wp-content/uploads/2019/06/slider_test_1.jpg" /> 
        </div>        
    </div>
  <div class="container">
    <div class="column-span">
      <p>Inspired by the golden age of transatlantic travel and the bounty of local fresh produce, our three distinctive food and drink venues offer an exciting and regularly changing experience – with views that pack a punch. Savour dishes that range from nourishing to hearty at Sea Containers Restaurant, which is perfect for an al fresco lunch by the river, sunset cocktails or an atmospheric dinner. Experience innovative cocktails in the sumptuously decorated and award-winning Dandelyan, with its sweeping green marble bar and distinctive pink seating. Take in panoramic views of the London skyline from our rooftop bar, 12th Knot, which features a glittering glass box and outdoor terrace for unhindered views over the River Thames.
      </p>  

      <div class="text-center">
          <a role="button" href="#" class="btn btn-brand-primary">View Food & Drink</a>
      </div>
    </div>
    <hr />
  </div>
</section>  

<section id="offers">
  <div class="container">

    <h2> Offers </h2>

    <div class="card-deck">

      <?php $args = array( 
          'post_type' => 'offers', 
          'types' => 'featured',
          'posts_per_page' => 2, 
          'order' => 'ASC'); $team = new WP_Query( $args );
          while ( $team->have_posts() ) : $team->the_post(); ?> 
        
          <div id="offer-<?php echo(get_the_ID()); ?>" class="card">
            <img class="card-img-top" src="<?php the_field('featured_image'); ?>" alt="<? the_title();?>">
            <div class="card-body">
              <h5 class="card-title"><?php the_title(); ?></h5>
              <p class="card-text"><?php the_field('intro_excerpt') ;?></p>
              <a role="button" href="#" class="excerpt-link">More Infomation</a>
            </div>
          </div>

      <?php endwhile; ?>
    
    </div>
  </div>
</section>

<div class="social_holder">
  <span>INSTAGRAM <i class="fab fa-instagram fa-lg"></i> @seacontainers</span>
  <?php echo do_shortcode("[elfsight_instagram_feed id=1]"); ?>
</div>

<?php endwhile; ?>